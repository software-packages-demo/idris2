import Data.Vect

vzipWith : (a -> b -> c) ->
           Vect n a -> Vect n b -> Vect n c
